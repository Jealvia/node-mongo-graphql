import mongoose from 'mongoose';

export async function connect() {
    try {
        await mongoose.connect('mongodb://localhost/mongodbgraphql', {
            userNewUrlParser: true
        })
        console.log('>>>Db is conected');
    } catch (error) {
        console.log(error);
    }


}